# Grid Beautification

Custom artwork (animated pngs, logos, headers) for the new Steam grid layout;
inspired by [/r/steamgrid](https://reddit.com/r/steamgrid)

[Browse all animated PNGs](Games/APNGs.md)

<img src="Games/Untitled Goose Game/Lake.png" width="200" height="300">
<img src="Games/Roof Rage/Roof Rage with shake.png" width="200" height="300">
<img src="Games/Minecraft/Forest Trader.png" width="200" height="300">
<img src="Games/Getting Over It with Bennett Foddy/Breathing.png" width="200" height="300">


# Installation

Download and run [Steam Artwork Manager](https://gitlab.com/avalonparton/steam-manager) to quickly apply artwork from this repo.

# Art Sources

Most of the artwork comes from in-game recordings, or is the official artwork from the game or promo materials.

Pictures are edited with [Photopea](https://www.photopea.com/), and the video editing is done in Adobe Premiere.

## Alternate installation method

You need [Python 3](https://www.python.org/downloads/) to auto-apply the art.

Clone this repo, or download [grid-beautification-master.zip](https://gitlab.com/avalonparton/grid-beautification/-/archive/master/grid-beautification-master.zip) and extract it somewhere.

Run `app.py` and press install to auto-apply all available artwork (WARNING: this will overwrite existing custom art)

![screenshot](https://i.imgur.com/5yDkZpc.png)