import tkinter as tk
from scripts import steam
import sys


class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Steam grid custom artwork installer by plomdawg")
        toolbar = tk.Frame(self)
        toolbar.pack(side="top", fill="x")

        # Window Dimensions
        self.width  = int(self.winfo_screenwidth()/4)
        self.height = int(self.winfo_screenheight()*3/6)
        self.geometry(f'{self.width}x{self.height}')

        #b1 = tk.Button(self, text="List games", command=self.list_games)
        b2 = tk.Button(self, text="Install", command=self.install)
        b3 = tk.Button(self, text="QUIT", command=sys.exit)
        #b1.pack(in_=toolbar, side="left")
        b3.pack(in_=toolbar, side="right")
        b2.pack(in_=toolbar, side="right")


        # Put the lists in a frame on the left
        self.f1 = tk.Frame()
        self.f1.pack(side="left", fill="y")

        tk.Label(self.f1, text="Installed Games").pack(side="top")
        self.text = tk.Text(self.f1, wrap="word")
        self.text.pack(side="top", fill="y", expand=True)
        self.text.tag_configure("game_list", foreground="#222282")
        
        tk.Label(self.f1, text="Non-steam Shortcuts").pack(side="top")
        self.text2 = tk.Text(self.f1, wrap="word")
        self.text2.pack(side="bottom", fill="y", expand=True)
        self.text2.tag_configure("shortcut_list", foreground="#222222")
        
        tk.Label(self, text="Install log").pack(side="top")
        self.text3 = tk.Text(self, wrap="word")
        self.text3.pack(side="right", fill="both", expand=True)
        self.text3.tag_configure("install_log", foreground="#228222")

        #sys.stdout = TextRedirector(self.text, "stdout")
        #sys.stderr = TextRedirector(self.text, "stderr")

        # Load Steam data
        self.user = steam.get_users()[0]
        self.shortcuts = steam.get_shortcuts(self.user)
        self.games = steam.get_installed_games()

        self.info()
        self.list_games()
        self.list_shortcuts()

    def write(self, tag, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (tag))
        self.widget.configure(state="disabled")

    def info(self):
        print(f"-------------------------------------------------")
        print(f" Steam grid custom artwork installer by plomdawg ")
        print(f"-------------------------------------------------")

    def list_games(self):
        sys.stdout = TextRedirector(self.text, "game_list")
        sys.stderr = TextRedirector(self.text, "game_list")
        print(f"Installed Games:")
        for game in self.games:
            print(f"  > {game}")
            self.text.see("end")

    def list_shortcuts(self):
        sys.stdout = TextRedirector(self.text2, "shortcut_list")
        sys.stderr = TextRedirector(self.text2, "shortcut_list")
        print(f"Non-steam Shortcuts:")
        for shortcut in self.shortcuts:
            print(f"  > {shortcut}")
            self.text.see("end")

    def install(self):
        """
        Downloads and installs custom artwork for all 
        installed games and custom shortcuts.
        """
        sys.stdout = TextRedirector(self.text3, "install_log")
        sys.stderr = TextRedirector(self.text3, "install_log")
        for game in self.games + self.shortcuts:
            self.text.see("end")
            steam.get_artwork(game, self.user)
            self.text.see("end")


# Adapted from https://stackoverflow.com/a/12352237
class TextRedirector(object):
    def __init__(self, widget, tag="stdout"):
        self.widget = widget
        self.tag = tag

    def write(self, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (self.tag,))
        self.widget.configure(state="disabled")

app = App()
app.mainloop()
