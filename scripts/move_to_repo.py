# Drop a folder onto this script to move all .pngs to the _repo/Games/[game] folder
import glob
import os
import sys
from shutil import copyfile

def alert(title, message):
    """ Pops up an message box """
    import tkinter
    from tkinter import messagebox
    root = tkinter.Tk()
    root.withdraw() # Hide main window
    messagebox.showinfo(title, message)

def main():
    # Script must be called with at least one argument
    if len(sys.argv ) == 1:
        alert("Error", "Please drag input file or folder onto this script")
        sys.exit(1)

    # Find all .png files within each folder
    input_files = []
    for path in sys.argv[1:]:
        if os.path.isdir(path):
            game = os.path.split(path)[-1] # folder name

            for png in glob.glob(path + "/*.png"):
                # Move to the git repo
                repo_path = "_repo/Games/" + game
                
                basename = os.path.basename(png)
                target = repo_path + "/" + basename
                if not os.path.isdir(repo_path):
                    os.makedirs(repo_path)

                # Leave logo and header in place
                if basename.startswith("logo") or basename.startswith("header"):
                    copyfile(png, target)
                # Move apngs to save disk space
                else:
                    os.rename(png, target)
        else:
            alert("Warning", f"Input is not a directory: {path}")


    print(f"Moved {len(input_files)} file(s)!")

if __name__ == "__main__":
    # execute only if run as a script
    main()
