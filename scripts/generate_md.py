import glob
import os
import sys
import string

def to_markdown_link(text):
    # Converts text to markdown links
    text = text.translate(str.maketrans('', '', string.punctuation))
    return "#" + text.lower().replace(" ", "-")

def generate_directory(games):
    # * [Unitled Goose Game](#untitled-goose-game)
    text = ""
    for game in games:
        text += f" * [{game}]({to_markdown_link(game)})"
        text += "\n"
    text += "\n"
    return text

def generate_list(games):
    # * ![Unitled Goose Game](APNGs/goosegame.png)
    text = ""
    for game in games:
        # Game title as subheader
        text += f"## {game}\n\n"

        # Find all .png files in the subdirectory
        logo = ""
        header = ""
        apngs = ""
        apng_count = 0
        for png in glob.glob(f"./Games/{game}/*.png"):
            file_name = os.path.basename(png)

            if file_name.startswith('logo'):
                logo += f'---\nLogo: <img src="{game}/{file_name}" height="175"><br>\n\n'
            elif file_name.startswith('header'):
                header += f'---\nHero: <img src="{game}/{file_name}" width="696" height="225"><br>\n\n'
            else:
                apngs += f'<img src="{game}/{file_name}" width="150" height="225">\n'
                apng_count += 1

        if apng_count == 1:
            apngs = f"---\nGrid: {apngs}"
        elif apng_count > 1:
            apngs = f"---\nGrids: {apngs}"

        text += logo + header + apngs + "\n"

    return text

def main():
        
    games = [g for g in os.listdir("./Games/") if os.path.isdir("./Games/"+g)]
    header = "# Animated PNGs\n"
    header += "Click the image to load the full size before saving!"
    directory = generate_directory(games)
    body = generate_list(games)

    with open("Games/APNGs.md", "w+") as f:
        f.write(header + "\n\n")
        f.write(directory + "\n\n")
        f.write(body)
    
    print("Generated: APNGs.md")


if __name__ == "__main__":
    # execute only if run as a script
    main()