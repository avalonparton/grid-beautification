import glob
import os
import subprocess
import sys
import tkinter
from tkinter import messagebox


def convert_to_apng(file_path, fps=30):
    """ Converts a video to an animated PNG.

    Args:
        file_path: Input video file path
        fps:       Desired output FPS

    Raises:
        ? - 

    Returns:
        Path to the output file
    """
    print(file_path)
    save_path = os.path.splitext(file_path)[0] + ".png"

    # FFMPEG arguments:
    # -f apng                 (Output filetype animated PNG)
    # -y                      (Overwrite existing files)
    # -plays 0                (Output does not loop)
    # palettegen / paletteuse (Downsample original video to a 255 color palette)
    command = f'ffmpeg -i "{file_path}" -y -f apng -plays 0 -vf "fps={fps},split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse[pp]" "{save_path}" -hide_banner'
    print("Running command:")
    print("      ", command)
    subprocess.call(command, shell=True)
    #alert("Success!", f"Created file: {save_path}")
    return save_path

def alert(title, message):
    """ Pops up an message box """
    root = tkinter.Tk()
    root.withdraw() # Hide main window
    messagebox.showinfo(title, message)

def main():
    # Script must be called with at least one argument
    if len(sys.argv ) == 1:
        alert("Error", "Please drag input file or folder onto this script")
        sys.exit(1)

    # Find all files within each argument
    input_files = []
    for path in sys.argv[1:]:
        if os.path.isfile(path):
            if (os.path.splitext(path)[1] == ".mp4") or (os.path.splitext(path)[1] == ".avi"):
                input_files.append(path)
            else:
                alert("Warning", f"Ignoring input (not an .mp4 or .avi): {path}")
        elif os.path.isdir(path):
            input_files += glob.glob(path + "**/*.mp4", recursive=True)
            input_files += glob.glob(path + "**/*.avi", recursive=True)
        else:
            alert("Warning", f"Ignoring input (non-existant): {path}")


    # Remove duplicates
    input_files = list(dict.fromkeys(input_files))
    n = len(input_files)

    if (n == 0):
        alert("Error", "Could not find any .mp4s or .avis!")
        sys.exit(1)

    print(f"Found {n} file(s)!")

    for f in input_files:
        convert_to_apng(f)

if __name__ == "__main__":
    # execute only if run as a script
    main()