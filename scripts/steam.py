#!python
import glob
import os
import re
import sys
import winreg
from shutil import copyfile
import zlib


APNG_PATH = ".\\Games"

# Find the Steam install directory or bust
try: # 32-bit
    key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\\Valve\\Steams")
except FileNotFoundError:
    try: # 64-bit
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Valve\\Steam")
    except FileNotFoundError as e:
        print("Failed to find Steam Install directory")
        raise(e)

STEAM_PATH = winreg.QueryValueEx(key, "InstallPath")[0]


class Game():
    def __init__(self, id, name):
        self.id = id
        self.name = name
        
    def __repr__(self):
        return f"{self.name} ({self.id})"

class Shortcut():
    def __init__(self, entry, user_id):
        self.user_id = user_id
        self._entry = entry.split(b'\x00')
        self.name = self._entry[0].decode()
        self.exe = self._entry[2].decode()
        self.start_dir = self._entry[4].decode()
        self._id = None
        #self.icon = self._entry[6].decode()
        #self.shortcut_path = self._entry[8].decode()
        #self.launch_options = self._entry[10].decode()
        #self.hidden = (self._entry[12] == b'\x01') # \x01 is true, \x00 is false
        #self.allow_desktop_config = (self._entry[16] == b'\x01')
        #self.allow_overlay = (self._entry[20] == b'\x01')
        #self.in_vr_library = (self._entry[24] == b'\x01')
        #self.devkit = (self._entry[28] == b'\x01')
        #self.devkit_game_id = self._entry[32].decode()
        #self.last_play_time = self._entry[22].decode()
        #self.tags = self._entry[24].decode()
        #self.info()
    
    def __repr__(self):
        return f"{self.name} ({self.id})"
        #return f"{self.name} ({self.id}) Target: {self.exe} in {self.start_dir}"

    @property
    def id(self):
        if self._id is None:
            s = self.exe + self.name
            self._id = str((zlib.crc32(s.encode()) & 0xffffffff) | 0x80000000)
        return self._id

    @property
    def apng_path(self):
        return f"{STEAM_PATH}\\userdata\\{self.user_id}"

def get_users():
    """ Returns a list of user ID's sorted by last login date """
    path = f"{STEAM_PATH}\\userdata\\*"
    files = glob.glob(path)
    files.sort(key=os.path.getmtime, reverse=True) # sort by modified date
    return [os.path.basename(f) for f in files]

def get_shortcuts(user_id):
    """ Returns a list of Shortcuts from a user's shortcuts.vdf """
    with open(f"{STEAM_PATH}\\userdata\\{user_id}\\config\\shortcuts.vdf", 'rb') as shortcuts_vdf:
        data = shortcuts_vdf.read()
    shortcuts = []
    for entry in data.split(b'\x01AppName\x00')[1:]:
        shortcuts.append(Shortcut(entry, user_id))
    return shortcuts

def get_steam_libraries():
    """ Returns a list of paths to the steam libraries found in 
    Steam\steamapps\libraryfolders.vdf
    """
    libraries = [STEAM_PATH]
    with open(f"{STEAM_PATH}\\steamapps\\libraryfolders.vdf", 'r') as f:
        lf = f.read()
        libraries.extend([fn.replace("\\\\", "\\") for fn in
            re.findall('^\s*"\d*"\s*"([^"]*)"', lf, re.MULTILINE)])
    return libraries

def get_installed_games():
    """ Returns a list of all installed Steam games as Game objects """
    games = []
    for library in get_steam_libraries():
        pattern = library + "\\steamapps\\appmanifest_*.acf"
        app_manifests = glob.glob(pattern)
        for manifest in app_manifests:
            app_id = manifest.split('appmanifest_')[-1][:-4]
            name = "Unknown" # placeholder
            with open(manifest, 'r') as f:
                while True:
                    line = f.readline()
                    if '"name"' in line:
                        name = line.split('"')[3]
                        break
                    if line == "":
                        break
            games.append(Game(app_id, name))
    return games

#def add_artwork()

def get_artwork(game, user):
    """ Finds and applies artwork for the given game """
    name = re.sub(r'[^\x00-\x7f]|/|\\', '', game.name).strip()
    path = f"{APNG_PATH}\\{name}"
    print(f"Searching for {game.name} artwork in '{path}'")

    if os.path.isdir(path):
        apngs = [f for f in glob.glob(path + "\\*.png")
                if not os.path.basename(f).startswith("logo")
                and not os.path.basename(f).startswith("header")]
        #print(f"Found custom artwork for {game.name} ({game.id}): {apngs[0]}")
        target = f"{STEAM_PATH}\\userdata\\{user}\\config\\grid\\" + game.id
        # Copy the animated cover
        if len(apngs) > 0:
            copyfile(apngs[0], target+"p.png")
            print(f"  ✔ Copied {target}p.png")
        # Copy the logo
        if os.path.isfile(path + "\\logo.png"):
            copyfile(path + "\\logo.png", target+"_logo.png")
            print(f"  ✔ Copied {target}_logo.png")
        # Copy the header
        if os.path.isfile(path + "\\header.png"):
            copyfile(path + "\\header.png", target+"_hero.png")
            print(f"  ✔ Copied {target}_hero.png")
    else:
        pass
        #print(f"Missing artwork for {game.name} ({game.id}). Request it!")

